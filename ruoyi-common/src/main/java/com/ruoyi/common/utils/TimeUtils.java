package com.ruoyi.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/6/30 17:01
 * @Description:
 */
public class TimeUtils {

    public static String formateDate2Str(Date date, String pattern) {
        SimpleDateFormat f = new SimpleDateFormat(pattern);
        return f.format(date);
    }

    public static Date convertString2Date(String pattern, String dateStr) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        ParsePosition pos = new ParsePosition(0);
        return formatter.parse(dateStr,pos);
    }


    /**
     *
     * 功能描述:将世界标准时间转成当前时区时间
     *
     * @param:
     * @return:
     * @auther: miaoguoxin
     * @date: 2019/6/22 0022 14:35
     */
    public static Date convertGMT2Local(Date gmtDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(gmtDate);
        int zoneOffset = c.get(Calendar.ZONE_OFFSET);
        int dstOffset = c.get(Calendar.DST_OFFSET);
        c.add(Calendar.MILLISECOND,zoneOffset+dstOffset);
        return c.getTime();
    }

    /**
     * 获取固定间隔时刻集合
     *
     * @param start    开始时间
     * @param end      结束时间
     * @param interval 时间间隔(单位：分钟)
     * @return
     */
    public static List<String> getIntervalTimeList(String start, String end, int interval,String pattern) {
        Date startDate = convertString2Date(pattern, start);
        Date endDate = convertString2Date(pattern, end);
        List<String> list = new ArrayList<>();
        while (Objects.requireNonNull(startDate).getTime() <= Objects.requireNonNull(endDate).getTime()) {
            list.add(formateDate2Str(startDate, pattern));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.add(Calendar.MINUTE, interval);
            if (calendar.getTime().getTime() > endDate.getTime()) {
                if (!startDate.equals(endDate)) {
                    list.add(formateDate2Str(endDate, pattern));
                }
                startDate = calendar.getTime();
            } else {
                startDate = calendar.getTime();
            }

        }
        return list;
    }

    /**
     * 给时间加上几个小时
     * @param day 当前时间 格式：yyyy-MM-dd HH:mm:ss
     * @param time 需要加的时间
     * @return
     */
    public static String addDateMinut(String day, int time,String pattern,int unit) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse(day);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (date == null)
            return "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(unit, time);// 24小时制
        date = cal.getTime();
        return format.format(date);

    }

    public static void main(String[] args) {
        String s = addDateMinut("22:22", -1, "HH:mm", Calendar.MINUTE);
        System.out.println(s);
    }
}
