package com.executor.gateway.model.po;

import com.executor.gateway.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/4/28 0028 17:49
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IpList extends BaseEntity {
    public static final int BLACK_TYPE = 1;//黑名单
    public static final int WHITE_TYPE = 2;//白名单
    /**
     * ip地址
     */
    private String ip;
    /**
     * ip名单类型,1:黑名单 2:白名单
     */
    private Integer ipListType;
}
