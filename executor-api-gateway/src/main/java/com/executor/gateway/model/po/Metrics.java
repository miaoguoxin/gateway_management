package com.executor.gateway.model.po;

import com.executor.gateway.model.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @version v1.0
 * @ClassName: Metrics
 * @Description: 统计用的实体
 * @author: miaoguoxin
 * @date: 2019/6/29 17:32
 */
@Data
public class Metrics extends BaseEntity {
    private String reqUri;
    private Integer totalCount = 0;
    private Integer successCount = 0;
    private Integer errCount = 0;
}
