package com.executor.gateway.mapper;

import com.executor.gateway.model.po.IpList;

import java.util.List;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/4/28 0028 17:41
 * @Description:
 */
public interface IpListMapper {
    List<IpList> queryAll();
}
