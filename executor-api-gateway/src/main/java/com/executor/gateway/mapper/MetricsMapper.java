package com.executor.gateway.mapper;

import com.executor.gateway.model.po.Metrics;

import java.util.List;

public interface MetricsMapper {

    int insertBatchMetrics(List<Metrics> metrics);
}
