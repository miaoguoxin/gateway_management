package com.executor.gateway.service;

import com.executor.gateway.core.ApiResult;
import com.executor.gateway.core.constant.RESPONSE;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @version v1.0
 * @ClassName: FallbackHandler
 * @Description:
 * @author: miaoguoxin
 * @date: 2019/6/29 16:39
 */
@Component
public class RouteHandler {

    public Mono<ServerResponse> fallback(){
        Mono<ApiResult> result = Mono.just(new ApiResult<>(RESPONSE.ERROR,"当前服务不可用"));
        return  ServerResponse.status(HttpStatus.SERVICE_UNAVAILABLE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(result,ApiResult.class);

    }
}
