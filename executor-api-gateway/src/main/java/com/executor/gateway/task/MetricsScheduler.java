package com.executor.gateway.task;

import com.executor.gateway.service.MetricsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;


/**
 * @version v1.0
 * @ClassName: MetricsScheduler
 * @Description: 统计数据定时任务
 * @author: miaoguoxin
 * @date: 2019/6/29 17:20
 */
@Component
@EnableScheduling
@Slf4j
public class MetricsScheduler {
    @Autowired
    private MetricsService metricsService;

    @Scheduled(fixedRate = 1000 * 60, initialDelay = 1000 * 60)
    public void metrics2Db() {
        metricsService.save2Db();
        //log.info("持久化数据结束");
    }
}
