package com.executor.gateway.config;

import com.executor.gateway.service.RouteHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

/**
 * @version v1.0
 * @ClassName: Router
 * @Description:
 * @author: miaoguoxin
 * @date: 2019/6/29 16:38
 */
@Configuration
public class Router {
    @Bean
    public RouterFunction<ServerResponse> routeCity(RouteHandler handler) {
        return RouterFunctions
                .route(RequestPredicates.path("/defaultfallback")
                                .and(RequestPredicates.accept(MediaType.APPLICATION_JSON_UTF8)),
                        request -> handler.fallback());
    }
}
