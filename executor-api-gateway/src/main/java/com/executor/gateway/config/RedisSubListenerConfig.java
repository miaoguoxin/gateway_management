package com.executor.gateway.config;

import com.executor.gateway.config.properties.RedisTopicProperties;
import com.executor.gateway.core.RedisTopicMessageReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/4/1 20:46
 * @Description: redis topic通道配置
 */
@Configuration
@EnableConfigurationProperties(RedisTopicProperties.class)
public class RedisSubListenerConfig {
    @Autowired
    private RedisTopicProperties redisTopicProperties;

    /**
     * redis消息监听器容器
     * 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     * @param connectionFactory
     * @param listenerAdapter
     * @return
     */
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter listenerAdapter, MessageListenerAdapter listenerAdapter2,MessageListenerAdapter listenerAdapter3) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        //订阅了一个刷新route的通道
        container.addMessageListener(listenerAdapter, new PatternTopic(redisTopicProperties.getRouteChannel()));
        //订阅了一个刷新api的通道
        container.addMessageListener(listenerAdapter2, new PatternTopic(redisTopicProperties.getApiChannel()));
        //订阅了一个刷新ipLists的通道
        container.addMessageListener(listenerAdapter3, new PatternTopic(redisTopicProperties.getIpListsChannel()));
        return container;
    }

    /**
     * 消息监听器适配器(gateway)
     * @param receiver
     * @return
     */
    @Bean
    MessageListenerAdapter listenerAdapter(RedisTopicMessageReceiver receiver) {
        return new MessageListenerAdapter(receiver, redisTopicProperties.getRouteMethodName());
    }
    /**
     * 消息监听器适配器(api)
     * @param receiver
     * @return
     */
    @Bean
    MessageListenerAdapter listenerAdapter2(RedisTopicMessageReceiver receiver) {
        return new MessageListenerAdapter(receiver, redisTopicProperties.getApiMethodName());
    }
    @Bean
    MessageListenerAdapter listenerAdapter3(RedisTopicMessageReceiver receiver) {
        return new MessageListenerAdapter(receiver, redisTopicProperties.getIpListsMethodName());
    }
}
