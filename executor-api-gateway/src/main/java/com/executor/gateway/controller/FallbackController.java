package com.executor.gateway.controller;

import com.executor.gateway.core.ApiResult;
import com.executor.gateway.core.constant.RESPONSE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.zookeeper.discovery.ZookeeperDiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/3/28 0028 17:53
 * @Description: 熔断Controller
 */
@RestController
@Slf4j
public class FallbackController {
    @Autowired
    private ZookeeperDiscoveryClient zookeeperDiscoveryClient;


    @GetMapping("/")
    public ApiResult<String> test()
    {
        return new ApiResult<>(RESPONSE.SUCCESS,"成功");
    }
    @GetMapping("/csrf")
    public ApiResult<String> csrf(){
        return new ApiResult<>(RESPONSE.SUCCESS,"csrf");
    }


    @RequestMapping("test/{id}")
    public ApiResult testRoute(@PathVariable String id){
        List<ServiceInstance> instances = zookeeperDiscoveryClient.getInstances(id);
        return new ApiResult(RESPONSE.SUCCESS,"成功",instances);
    }



    @GetMapping(value = "/test/flux",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> flux(){
        Flux<String> result = Flux
                .fromStream(IntStream.range(1, 5).mapToObj(i -> {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                    }
                    return "flux data--" + i;
                }));
        return result;
    }
}
