package com.executor.gateway.core.rabbit;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/4/17 0017 10:04
 * @Description:
 */
@Component
@Slf4j
public class RabbitSender{
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Value("${mq.exchange}")
    private String payExchange;

    @Value("${mq.ingate.queue}")
    private String ingateQueue;

   // @Transactional
    public void sendIngateQueue(JSONObject msg) {
        log.info("进闸支付消息已发送 {}",msg.toJSONString());
        for (int i = 0; i < 50; i++) {
            CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
            log.info("消息id:" + correlationData.getId());
            rabbitTemplate.convertAndSend(payExchange, ingateQueue, msg.toJSONString(), correlationData);
        }
    }

}
