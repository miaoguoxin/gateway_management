package com.ruoyi.web.controller.config;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.MetricsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/6/29 21:05
 * @Description:
 */
@RequestMapping("/system/metrics")
@Controller
public class MetricsController {
    @Autowired
    private MetricsService metricsService;

    private String prefix = "system/metrics";

    @RequiresPermissions("system:metrics:view")
    @GetMapping()
    public String metricsView()
    {
        return prefix + "/metrics";
    }

    @GetMapping("/list/{type}")
    @ResponseBody
    //type:1:24小时内 2:3天内 3:7天内
    public Map<String,Object> list(@PathVariable Integer type){
        return metricsService.list(type);
    }
}
