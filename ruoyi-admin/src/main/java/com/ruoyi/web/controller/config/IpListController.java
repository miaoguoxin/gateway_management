package com.ruoyi.web.controller.config;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.IpList;
import com.ruoyi.system.service.IIpListService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * ip名单(黑白名单) 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-04-28
 */
@Controller
@RequestMapping("/system/ipList")
public class IpListController extends BaseController
{
    private String prefix = "system/ipList";
	
	@Autowired
	private IIpListService ipListService;
	
	@RequiresPermissions("system:ipList:view")
	@GetMapping()
	public String ipList()
	{
	    return prefix + "/ipList";
	}
	
	/**
	 * 查询ip名单(黑白名单)列表
	 */
	@RequiresPermissions("system:ipList:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(IpList ipList)
	{
		startPage();
        List<IpList> list = ipListService.selectIpListList(ipList);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出ip名单(黑白名单)列表
	 */
	@RequiresPermissions("system:ipList:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(IpList ipList)
    {
    	List<IpList> list = ipListService.selectIpListList(ipList);
        ExcelUtil<IpList> util = new ExcelUtil<IpList>(IpList.class);
        return util.exportExcel(list, "ipList");
    }
	
	/**
	 * 新增ip名单(黑白名单)
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存ip名单(黑白名单)
	 */
	@RequiresPermissions("system:ipList:add")
	@Log(title = "ip名单(黑白名单)", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(IpList ipList)
	{		
		return toAjax(ipListService.insertIpList(ipList));
	}

	/**
	 * 修改ip名单(黑白名单)
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap)
	{
		IpList ipList = ipListService.selectIpListById(id);
		mmap.put("ipList", ipList);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存ip名单(黑白名单)
	 */
	@RequiresPermissions("system:ipList:edit")
	@Log(title = "ip名单(黑白名单)", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(IpList ipList)
	{		
		return toAjax(ipListService.updateIpList(ipList));
	}
	
	/**
	 * 删除ip名单(黑白名单)
	 */
	@RequiresPermissions("system:ipList:remove")
	@Log(title = "ip名单(黑白名单)", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ipListService.deleteIpListByIds(ids));
	}
	
}
