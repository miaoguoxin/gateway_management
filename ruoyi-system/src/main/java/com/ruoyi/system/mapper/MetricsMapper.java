package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Metrics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/6/30 16:41
 * @Description:
 */
public interface MetricsMapper {
    List<Metrics> queryBetweenTime(@Param("startTime") String startTime,@Param("endTime") String endTime);
}
