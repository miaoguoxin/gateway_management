package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.config.RedisTopicChannelProperties;
import com.ruoyi.system.listener.GatewayRefreshEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.IpListMapper;
import com.ruoyi.system.domain.IpList;
import com.ruoyi.system.service.IIpListService;
import com.ruoyi.common.core.text.Convert;

/**
 * ip名单(黑白名单) 服务层实现
 * 
 * @author ruoyi
 * @date 2019-04-28
 */
@Service
public class IpListServiceImpl implements IIpListService 
{
	@Autowired
	private IpListMapper ipListMapper;
	@Autowired
	protected RedisTopicChannelProperties redisTopicChannelProperties;
	@Autowired
	private ApplicationContext applicationContext;

	/**
     * 查询ip名单(黑白名单)信息
     * 
     * @param id ip名单(黑白名单)ID
     * @return ip名单(黑白名单)信息
     */
    @Override
	public IpList selectIpListById(Long id)
	{
	    return ipListMapper.selectIpListById(id);
	}
	
	/**
     * 查询ip名单(黑白名单)列表
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return ip名单(黑白名单)集合
     */
	@Override
	public List<IpList> selectIpListList(IpList ipList)
	{
	    return ipListMapper.selectIpListList(ipList);
	}
	
    /**
     * 新增ip名单(黑白名单)
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return 结果
     */
	@Override
	public int insertIpList(IpList ipList)
	{
		int result = ipListMapper.insertIpList(ipList);
		if(result>0){
			this.sendRefreshAllRouteNotify();
		}
		return result;
	}
	
	/**
     * 修改ip名单(黑白名单)
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return 结果
     */
	@Override
	public int updateIpList(IpList ipList)
	{
		int result = ipListMapper.updateIpList(ipList);
		if (result>0){
			this.sendRefreshAllRouteNotify();
		}
		return result;
	}

	/**
     * 删除ip名单(黑白名单)对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteIpListByIds(String ids)
	{
		return ipListMapper.deleteIpListByIds(Convert.toStrArray(ids));
	}

	private void sendRefreshAllRouteNotify(){
		applicationContext.publishEvent(new GatewayRefreshEvent(this,redisTopicChannelProperties.getIpListsChannel()));
	}
}
