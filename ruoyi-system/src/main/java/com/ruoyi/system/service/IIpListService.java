package com.ruoyi.system.service;

import com.ruoyi.system.domain.IpList;
import java.util.List;

/**
 * ip名单(黑白名单) 服务层
 * 
 * @author ruoyi
 * @date 2019-04-28
 */
public interface IIpListService 
{
	/**
     * 查询ip名单(黑白名单)信息
     * 
     * @param id ip名单(黑白名单)ID
     * @return ip名单(黑白名单)信息
     */
	public IpList selectIpListById(Long id);
	
	/**
     * 查询ip名单(黑白名单)列表
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return ip名单(黑白名单)集合
     */
	public List<IpList> selectIpListList(IpList ipList);
	
	/**
     * 新增ip名单(黑白名单)
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return 结果
     */
	public int insertIpList(IpList ipList);
	
	/**
     * 修改ip名单(黑白名单)
     * 
     * @param ipList ip名单(黑白名单)信息
     * @return 结果
     */
	public int updateIpList(IpList ipList);
		
	/**
     * 删除ip名单(黑白名单)信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteIpListByIds(String ids);
	
}
