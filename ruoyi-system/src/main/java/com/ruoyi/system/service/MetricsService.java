package com.ruoyi.system.service;


import java.util.Map;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/6/30 12:40
 * @Description:
 */
public interface MetricsService {
    Map<String,Object> list(Integer type);
}
