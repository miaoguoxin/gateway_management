package com.ruoyi.system.domain.bo;

/**
 * @version v1.0
 * @ClassName: MetricsCountBo
 * @Description: TODO
 * @author: miaoguoxin
 * @date: 2019/7/1 11:20
 */
public class MetricsCountBo {
    private int total;
    private int err;
    private int success;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
