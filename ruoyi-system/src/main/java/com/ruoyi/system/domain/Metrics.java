package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * @Auther: miaoguoxin
 * @Date: 2019/6/30 12:10
 * @Description:
 */
public class Metrics extends BaseEntity {
    public static final int IN_ONE_HOUR=1;
    public static final int IN_ONE_DAY=2;
    public static final int IN_THREE_DAY=3;
    public static final int IN_ONE_WEEK=4;
    private Long id;
    private String reqUri;
    private Integer totalCount;
    private Integer successCount;
    private Integer errCount;
    //用来分组的时间
    private String groupByTime;

    public String getGroupByTime() {
        return groupByTime;
    }

    public void setGroupByTime(String groupByTime) {
        this.groupByTime = groupByTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReqUri() {
        return reqUri;
    }

    public void setReqUri(String reqUri) {
        this.reqUri = reqUri;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getErrCount() {
        return errCount;
    }

    public void setErrCount(Integer errCount) {
        this.errCount = errCount;
    }
}
