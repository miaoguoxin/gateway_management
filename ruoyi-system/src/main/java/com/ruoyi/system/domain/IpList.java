package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * ip名单(黑白名单)表 t_ip_list
 * 
 * @author ruoyi
 * @date 2019-04-28
 */
public class IpList extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 主键Id */
	private Long id;
	/** ip地址 */
	private String ip;
	/** ip名单类型,1:黑名单 2:白名单 */
	private Integer ipListType;
	/** 创建时间 */
	private Date createTime;
	/** 修改时间 */
	private Date updateTime;
	/** 状态 */
	private Integer state;

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getId() 
	{
		return id;
	}
	public void setIp(String ip) 
	{
		this.ip = ip;
	}

	public String getIp() 
	{
		return ip;
	}
	public void setIpListType(Integer ipListType) 
	{
		this.ipListType = ipListType;
	}

	public Integer getIpListType() 
	{
		return ipListType;
	}
	public void setCreateTime(Date createTime) 
	{
		this.createTime = createTime;
	}

	public Date getCreateTime() 
	{
		return createTime;
	}
	public void setUpdateTime(Date updateTime) 
	{
		this.updateTime = updateTime;
	}

	public Date getUpdateTime() 
	{
		return updateTime;
	}
	public void setState(Integer state) 
	{
		this.state = state;
	}

	public Integer getState() 
	{
		return state;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ip", getIp())
            .append("ipListType", getIpListType())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("state", getState())
            .toString();
    }
}
