package com.ruoyi.system.domain.bo;

import java.io.Serializable;

/**
 * @version v1.0
 * @ClassName: MetricsTypeBo
 * @Description:
 * @author: miaoguoxin
 * @date: 2019/7/1 14:11
 */

public class MetricsTypeBo implements Serializable {
    //间隔时间 单位:分
    private int interval;
    private String startTime;
    private String endTime;
    private String xDatePattern;

    public String getxDatePattern() {
        return xDatePattern;
    }

    public void setxDatePattern(String xDatePattern) {
        this.xDatePattern = xDatePattern;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
